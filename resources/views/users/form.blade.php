<div class="position-relative row form-group {{ $errors->has('name') ? 'has-error' : ''}}">
    <label for="nama" class="col-sm-2 col-form-label text-bold">{{ 'Nama' }}</label>
    <div class="col-lg-10">
        <input class="form-control" autocomplete="off" name="name" type="text" id="name" value="{{ isset($user)?old('name', $user->name):old('name')}}" >
    </div>
</div>
<div class="row">
  <div class="col-sm-2"></div><div class="col-sm-10">{!! $errors->first('name', '<p class="help-block">:message</p>') !!}</div>
</div>
<div class="position-relative row form-group {{ $errors->has('username') ? 'has-error' : ''}}">
    <label for="nama" class="col-sm-2 col-form-label text-bold">{{ 'Username' }}</label>
    <div class="col-lg-10">
        <input class="form-control" autocomplete="off" name="username" type="text" id="username" value="{{ isset($user)?old('username', $user->username):old('username')}}" >
    </div>
</div>
<div class="row">
  <div class="col-sm-2"></div><div class="col-sm-10">{!! $errors->first('username', '<p class="help-block">:message</p>') !!}</div>
</div>
<div class="position-relative row form-group {{ $errors->has('email') ? 'has-error' : ''}}">
    <label for="nama" class="col-sm-2 col-form-label text-bold">{{ 'Email' }}</label>
    <div class="col-lg-10">
        <input class="form-control" autocomplete="off" name="email" type="email" id="email" value="{{ isset($user)?old('email', $user->email):old('email')}}" >
    </div>
</div>
<div class="row">
  <div class="col-sm-2"></div><div class="col-sm-10">{!! $errors->first('email', '<p class="help-block">:message</p>') !!}</div>
</div>
<div class="position-relative row form-group {{ $errors->has('password') ? 'has-error' : ''}}">
    <label for="nama" class="col-sm-2 col-form-label text-bold">{{ 'Sandi' }}</label>
    <div class="col-lg-10">
        <input class="form-control" autocomplete="off" name="password" type="password" id="password" value="" >
    </div>
</div>
<div class="row">
  <div class="col-sm-2"></div><div class="col-sm-10">{!! $errors->first('password', '<p class="help-block">:message</p>') !!}</div>
</div>
<div class="position-relative row form-group {{ $errors->has('password_confirmation') ? 'has-error' : ''}}">
    <label for="nama" class="col-sm-2 col-form-label text-bold">{{ 'Ulangi Sandi' }}</label>
    <div class="col-lg-10">
        <input class="form-control" autocomplete="off" name="password_confirmation" type="password" id="password_confirmation" value="" >
    </div>
</div>
<div class="row">
  <div class="col-sm-2"></div><div class="col-sm-10">{!! $errors->first('password_confirmation', '<p class="help-block">:message</p>') !!}</div>
</div>
<div class="position-relative row form-group {{ $errors->has('rules') ? 'has-error' : ''}}">
    <label for="nama" class="col-sm-2 col-form-label text-bold">{{ 'Rule' }}</label>
    <div class="col-lg-10">
      <select class="form-control selectpicker" name="rules[]" multiple="multiple">
    		@foreach(isset($rules)?$rules:array() as $item)
          <option value="{{$item->id}}" {{(isset($user) && old('rules', $user->rules->toArray('id'))==$item->id)||old('rules')==$item->id?'selected="selected"':''}}>{{$item->nama}}</option>
    		@endforeach
      </select>
    </div>
</div>
<div class="row">
  <div class="col-sm-2"></div><div class="col-sm-10">{!! $errors->first('rules', '<p class="help-block">:message</p>') !!}</div>
</div>
@section('js')
<script type="text/javascript">
  $('.selectpicker').select2();
</script>
@endsection
