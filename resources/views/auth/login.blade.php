@extends('master.masterauth')
@section('csstambahan')
@endsection
@section('content')
<div class="form-login">
		<p class="title">
				Aplikasi Inventaris Bappeko
		</p>
		<p class="sub-title">
				Selamat datang kembali, silahkan masuk
		</p>
		<form method="POST" id="formLogin" method="post" action="{{url('login')}}">
			@csrf
				<div class="username form-group">
						<label for="">Username</label>
						<input type="text" class="form-control" name="username" placeholder="masukkan username" autocomplete="off" required>
				</div>
				<div class="password form-group">
						<label for="">Password</label>
						<input type="password" class="form-control" name="password" placeholder="masukkan password" autocomplete="off" required>
				</div>
				<div class="button">
						<button type="submit" class="btn btn-green-cta btn-block  btn-shadow-login-invert loginbutton" id="btnLogin">
								<h5>Login</h5>
						</button>
				</div>
		</form>
</div>
@endsection
@section('footer')
<div class="footer-bottom">
	<div class="container">
		<div class="row">
			<div class="col-md-12 col-sm-12">
				<p>©2019 &middot;<a href="javascript:;"> Badan Perencanaan Pembangunan Kota Surabaya </a> &middot; All rights reserved</p>
			</div>
		</div>
	</div>
</div>
@endsection
@section('scripttambahan')

@endsection
