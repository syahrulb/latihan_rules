@extends('master.tamplate')
@section('judul_header','Rule')
@section('deskripsi_header','Berikut ini adalah rule dari sistem kajian')
@section('rules','mm-active')
@section('csstambahan')
@endsection
@section('btnAksi_header')
<a href="{{ url('/rules/create') }}" type="button" data-toggle="tooltip" title="" data-placement="bottom" class="btn-shadow mr-3 btn btn-success" data-original-title="Tambah Data">
    <i class="fas fa-plus-circle"></i>
</a>
@endsection
@section('content')
<div class="tabs-animation">
  <div class="row">
    <div class="col-md-12">
      <div class="main-card mb-3 card ">
        <div class="card-header">
          Rule
          <div class="btn-actions-pane-right">
            {{--<!-- button untuk tampil toggle -->--}}
            <div class="btn-group dropdown">
              <button type="button"  data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="btn-icon btn-icon-only btn btn-link"><i class="pe-7s-menu btn-icon-wrapper"></i></button>
              <div id="dropdown-menu-header" tabindex="-1" role="menu" aria-hidden="true" class="dropdown-menu-right rm-pointers dropdown-menu-shadow dropdown-menu-hover-link dropdown-menu" x-placement="bottom-end" style="position: absolute; transform: translate3d(-212px, 35px, 0px); top: 0px; left: 0px; will-change: transform;"><h6 tabindex="-1" class="dropdown-header">Header</h6>
                  <button type="button" tabindex="0" class="dropdown-item"><i class="dropdown-icon lnr-inbox"> </i><span>Menus</span></button>
                  <button type="button" tabindex="0" class="dropdown-item"><i class="dropdown-icon lnr-file-empty"> </i><span>Settings</span></button>
                  <button type="button" tabindex="0" class="dropdown-item"><i class="dropdown-icon lnr-book"> </i><span>Actions</span></button>
                  <div tabindex="-1" class="dropdown-divider"></div>
              </div>
            </div>
          </div>
        </div>
        <div class="card-body">
          <table style="width:100%;" id="tbl_form" class="table table-hover table-striped table-bordered">
            <thead>
              <tr>
                <th>#</th>
                <th>Jabatan / Rules </th>
                <th>status</th>
                <th>Aksi</th>
              </tr>
            </thead>
            <tbody>
              @foreach($rules as $item)
              <tr>
                <td>{{ $loop->iteration}}</td>
                <td>{{ $item->nama}}</td>
                <td>{!! ($item->status===1? '<span class="label label-success">Aktif</span> ': '<span class="label label-danger">Tidak Aktif</span>') !!}</td>
                <td>
                  <a href="{{ url('/rules/' . $item->id. '/edit') }}" data-toggle="tooltip" title="" data-original-title="Ubah Data" class="mr-2 btn-icon btn-icon-only btn btn-outline-primary"><i class="fa fa-edit"></i></a>
                  <a class="mr-2 btn-icon btn-icon-only btn btn-outline-danger" data-popup="tooltip" title="Hapus" href="{{ url('/rules' . '/' . $item->id) }}" onclick="event.preventDefault();document.getElementById('delete-{{$item->id}}').submit();"><i class="pe-7s-trash btn-icon-wrapper"></i></a></li>
                  <form method="POST" id="delete-{{$item->id}}" action="{{ url('/rules' . '/' . $item->id) }}" accept-charset="UTF-8" style="display:inline">
                    {{ method_field('DELETE') }}
                    {{ csrf_field() }}
                  </form>
                </td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
        <div class="d-block text-center card-footer">
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
@section('modal')
@endsection
@section('scripttambahan')
<script type="text/javascript">
$('#tbl_form').DataTable({
  autoWidth: true,
  columnDefs: [
    { "orderable": false, "targets":  [ 3 ] },
    { "searchable": false, "targets":  [ 3 ] }
  ],
  language: {
    "lengthMenu": "Tampilan _MENU_ Halaman",
    "zeroRecords": "Belum ada data yang disimpan ",
    "info": "Halaman _PAGE_ dari _PAGES_",
    "infoEmpty": "maaf tidak ada data yang tersimpan",
    "infoFiltered": "(saringan _MAX_ total catatan)",
    "search": " Cari : ",
    "placeholder": "Cari...",
    "paginate": {
              "previous": "sebelum",
              "next": "sesudah"
              }
    }
});
</script>
@endsection
