<div class="position-relative row form-group {{ $errors->has('nama') ? 'has-error' : ''}}">
    <label for="nama" class="col-sm-2 col-form-label text-bold">{{ 'Nama' }}</label>
    <div class="col-lg-10">
        <input class="form-control" autocomplete="off" name="nama" type="text" id="nama" value="{{ isset($rule)?old('nama', $rule->nama):old('nama')}}" >
    </div>
</div>
<div class="row">
  <div class="col-sm-2"></div><div class="col-sm-10">{!! $errors->first('nama', '<p class="help-block">:message</p>') !!}</div>
</div>
<div class="position-relative row form-group {{ $errors->has('status') ? 'has-error' : ''}}">
  <label for="nama" class="col-sm-2 col-form-label text-bold">{{ 'Status :' }}</label>
  <div class="col-lg-10">
    <div class="custom-radio custom-control">
      <input value="1" type="radio" id="exampleCustomRadio" name="status" class="custom-control-input" {{(isset($rule) &&old('status', $rule->status)===1) || old('status')===1?'checked':''}}>
      <label class="custom-control-label" for="exampleCustomRadio"> Aktif</label>
    </div>
    <div class="custom-radio custom-control">
      <input value="0" type="radio" id="exampleCustomRadio2" name="status" class="custom-control-input" {{(isset($rule) &&old('status', $rule->status)===1) || old('status')===0?'checked':''}}>
      <label class="custom-control-label" for="exampleCustomRadio2">Tidak Aktif</label>
    </div>
  </div>
</div>
<div class="row">
  <div class="col-sm-2"></div><div class="col-sm-10 col-sm-offset-2">{!! $errors->first('status', '<p class="help-block">:message</p>') !!}</div>
</div>
