@extends('master.tamplate')
@section('judul_header','Rule - Edit')
@section('deskripsi_header','Berikut ini adalah form untuk merubah rule')
@section('rules','mm-active')
@section('csstambahan')
@endsection
@section('btnAksi_header')
<a href="{{ url('/rules/create') }}" type="button" data-toggle="tooltip" title="" data-placement="bottom" class="btn-shadow mr-3 btn btn-success" data-original-title="Tambah Data">
    <i class="fas fa-plus-circle"></i>
  </a>
@endsection
@section('content')
<div class="tabs-animation">
  <div class="row">
    <div class="col-md-12">
      <div class="main-card mb-3 card ">
        <div class="card-header">
          Rule
          <div class="btn-actions-pane-right">
            {{--<!-- button untuk tampil toggle -->--}}
          </div>
        </div>
        <form method="POST" action="{{ url('/rules/' . $rule->id) }}" accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data">
          <div class="card-body">
            {{ method_field('PATCH') }}
            {{ csrf_field() }}
            @include ('rules.form', ['formMode' => 'edit'])
          </div>
          <div class="d-block text-center card-footer">
            <div class="form-group">
              <div class="row">
                <div class="col-lg-2">
                  <a href="{{url('rules')}}" data-toggle="tooltip" title="Kembali" data-original-title="Kembali" class="btn btn-social-icon btn-warning float-left"><i class="fa fa-arrow-left"></i></a>
                </div>
                <div class="col-lg-10">
                  <button id="btn_simpan" data-toggle="tooltip" title="Ubah" data-original-title="Ubah" type="submit" class="btn btn-social-icon btn-success float-right"><i class="fa fa-save"></i> </button>
                </div>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
@endsection
@section('modal')
@endsection
@section('scripttambahan')
<script type="text/javascript">
</script>
@endsection
