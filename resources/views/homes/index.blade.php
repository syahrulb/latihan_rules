@extends('master.tamplate')
@section('judul_header','Home')
@section('deskripsi_header','Berikut ini adalah statistik dari sistem kajian')
@section('homes','mm-active')
@section('csstambahan')
@endsection
@section('btnAksi_header')
<!-- <button type="button" data-toggle="tooltip" title="" data-placement="bottom" class="btn-shadow mr-3 btn btn-success" data-original-title="Tambah Data">
    <i class="fas fa-plus-circle"></i>
</button> -->
@endsection
@section('content')
<div class="tabs-animation">
  <div class="row">
    <div class="col-md-12">
      <div class="main-card mb-3 card ">
        <div class="card-header">
          Home
          <div class="btn-actions-pane-right">
            {{--<!-- button untuk tampil toggle -->--}}
            <div class="btn-group dropdown">
              <button type="button"  data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="btn-icon btn-icon-only btn btn-link"><i class="pe-7s-menu btn-icon-wrapper"></i></button>
              <div id="dropdown-menu-header" tabindex="-1" role="menu" aria-hidden="true" class="dropdown-menu-right rm-pointers dropdown-menu-shadow dropdown-menu-hover-link dropdown-menu" x-placement="bottom-end" style="position: absolute; transform: translate3d(-212px, 35px, 0px); top: 0px; left: 0px; will-change: transform;"><h6 tabindex="-1" class="dropdown-header">Header</h6>
                  <button type="button" tabindex="0" class="dropdown-item"><i class="dropdown-icon lnr-inbox"> </i><span>Menus</span></button>
                  <button type="button" tabindex="0" class="dropdown-item"><i class="dropdown-icon lnr-file-empty"> </i><span>Settings</span></button>
                  <button type="button" tabindex="0" class="dropdown-item"><i class="dropdown-icon lnr-book"> </i><span>Actions</span></button>
                  <div tabindex="-1" class="dropdown-divider"></div>
              </div>
            </div>
          </div>
        </div>
        <div class="card-body">
        </div>
        <div class="d-block text-center card-footer">
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
@section('modal')
@endsection
@section('scripttambahan')
@endsection
