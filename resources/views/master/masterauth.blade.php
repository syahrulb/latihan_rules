<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>{{ config('app.name') }}</title>
	{{--<!-- Global stylesheets -->---}}
	<link href="{{asset('assets/login/css/bootstrap.css')}}" rel="stylesheet" type="text/css">
	<link href="{{asset('assets/login/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css">
	<link href="{{asset('assets/login/css/main.css')}}" rel="stylesheet" type="text/css">
	<link href="{{asset('assets/login/css/main-login.css')}}" rel="stylesheet" type="text/css">
	<link href="{{asset('assets/login/css/main-login.css')}}" rel="stylesheet" type="text/css">
	{{--<!-- /global stylesheets -->--}}
	<link rel="shortcut icon" href="{{asset('assets/login/logo-sby.png')}}" />
	@yield('csstambahan')
</head>
<body>
  <div class="body">
      <div class="login">
          <div class="row">
              <div class="col-lg-7 left">
                  <img src="{{asset('assets/login/illus_login.png')}}" alt="" srcset="">
              </div>
              <div class="col-lg-5 bg-white-over">
                  <div class="login-wrapper">
                      <div class="top-logo">
                          <span>
                              <img src="{{asset('assets/login/logo-sby.png')}}" alt="Bappeko Surabaya">
                          </span>
                          <span> <p>Badan Perencanaan Pembangunan Kota Surabaya </p></span>
                      </div>
											@yield('content')
                  </div>
									@yield('footer')
              </div>
          </div>
      </div>
  </div>
	{{--<!-- Global Plugin Js -->--}}
	<script src="{{asset('plugins/sweetalert/sweetalert.min.js')}}"></script>
	<script src="{{asset('assets/login/js/jquery.js')}}"></script>
	<script src="{{asset('assets/login/js/bootstrap.js')}}"></script>
	<script src="{{asset('assets/login/js/jquery.sticky.js')}}"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
  {{--<!-- SweetAlert Plugin Js -->--}}
  <script src="{{asset('plugins/sweetalert/sweetalert.min.js')}}"></script>
	@yield('scripttambahan')
  @include('sweet::alert')
	@if (Session::has('berhasil'))
	<script>swal("Success!", "{{ Session('berhasil') }}", "success");</script>
	@elseif(Session::has('gagal'))
	<script>swal("Success!", "{{ Session('gagal') }}", "success");</script>
	@elseif(Session::has('peringatan'))
	<script>swal("Peringatan!", "{{ Session('peringatan') }}", "success");</script>
	@elseif(Session::has('informasi'))
	<script>swal("Informasi!", "{{ Session('informasi') }}", "warning");</script>
	@endif
</body>
</html>
