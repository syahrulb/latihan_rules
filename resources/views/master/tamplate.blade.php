<!doctype html>
<html >
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=Edge">
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <meta name="author" content="@bappeko | Syahrul Bastomy">
        <meta name="description" content="Website bagi dinas kota surabaya">
      	<meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="twitter:card" content="summary">
        <meta name="twitter:site" content="@bappeko">
        <meta name="twitter:creator" content="@bappeko">
        <meta name="twitter:title" content="E-kajian">
        <meta name="twitter:description" content="Website mengetahui kajian yang ada di kota surabaya ">
        <meta name="twitter:url" content="https://bappeko.surabaya.go.id/kajian">
      	<title>{{ config('app.name') }}</title>
        {{--<!-- Global stylesheets -->--}}
          <link rel="stylesheet" href="{{ asset('global.css')}}">
      	{{--<!-- /global stylesheets -->--}}
        {{--<!-- Fonts -->--}}
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
        <link href="{{asset('plugins/sweetalert/sweetalert.css')}}" rel="stylesheet" />
        <link href="{{asset('plugins/selects/select2.min.css')}}" rel="stylesheet" />
        <link href="{{asset('plugins/datatable/datatable.min.css')}}" rel="stylesheet" />
        {{--<!-- Styles -->--}}
        @yield('csstambahan')
    </head>
    <body>
      <div class="app-container app-theme-white body-tabs-shadow fixed-header fixed-sidebar" id="app">
        @include('master.layout.navbar')
        <div  class="app-main" id="app">
          @include('master.layout.sidebar')
          <div class="app-main__outer">
            <div class="app-main__inner">
              @include('master.layout.header')
              @yield('content')
            </div>
            @include('master.layout.footer')
          </div>
        </div>
      </div>
      @yield('modal')
      {{--<!-- Core JS files -->--}}
      <script type="text/javascript" src="{{asset('assets/jquery.min.js')}}"></script>
      <script type="text/javascript" src="{{asset('assets/scripts/global.js')}}"></script>
      {{--<!-- Core JS files --> --}}
      {{--<!-- SweetAlert Plugin Js -->--}}
      <script type="text/javascript" src="{{asset('plugins/sweetalert/sweetalert.min.js')}}"></script>
      <script type="text/javascript" src="{{asset('plugins/datatable/datatable.min.js')}}"></script>
      <script type="text/javascript" src="{{asset('plugins/selects/select2.min.js')}}"></script>
      <script type="text/javascript">
        $(document).ready(function() {
          {{--/* =========== BEGIN AJAX HEADER SECTION ================ */--}}
          $.ajaxSetup({headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}});
          {{--/* =========== END AJAX HEADER SECTION ================ */--}}
          if ($('.dataTables_length select') || $('.dataTables_filter input')) {
            $('.dataTables_length select').addClass('custom-select custom-select-sm form-control form-control-sm');
            $('.dataTables_filter input').addClass('form-control form-control-sm');
          }
        });
      </script>
      @yield('js')
      @yield('scripttambahan')
      @if (Session::has('berhasil'))
      <script>swal("Success!", "{{ Session('berhasil') }}", "success");</script>
      @elseif(Session::has('gagal'))
      <script>swal("Gagal!", "{{ Session('gagal') }}", "error");</script>
      @elseif(Session::has('peringatan'))
      <script>swal("Peringatan!", "{{ Session('peringatan') }}", "warning");</script>
      @elseif(Session::has('informasi'))
      <script>swal("Informasi!", "{{ Session('informasi') }}", "info");</script>
      @endif
    </body>
</html>
