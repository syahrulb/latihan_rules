<!-- Header -->
<div class="app-page-title">
    <div class="page-title-wrapper">
      <div class="page-title-heading">
          <div class="page-title-icon">
              <i class="pe-7s-date icon-gradient bg-mean-fruit">
              </i>
          </div>
          <div>@yield('judul_header')
              <div class="page-title-subheading">@yield('deskripsi_header')
              </div>
          </div>
      </div>
        <div class="page-title-actions">
          @yield('btnAksi_header')
        </div>
    </div>
</div>
