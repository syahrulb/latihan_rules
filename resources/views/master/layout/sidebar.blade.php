<div class="app-sidebar sidebar-shadow">
    <div class="app-header__logo">
        <div class="logo-src"></div>
        <div class="header__pane ml-auto">
            <div>
                <button type="button" class="hamburger close-sidebar-btn hamburger--elastic" data-class="closed-sidebar">
                    <span class="hamburger-box">
                        <span class="hamburger-inner"></span>
                    </span>
                </button>
            </div>
        </div>
    </div>
    <div class="app-header__mobile-menu">
        <div>
            <button type="button" class="hamburger hamburger--elastic mobile-toggle-nav">
                <span class="hamburger-box">
                    <span class="hamburger-inner"></span>
                </span>
            </button>
        </div>
    </div>
    <div class="app-header__menu">
        <span>
            <button type="button" class="btn-icon btn-icon-only btn btn-primary btn-sm mobile-toggle-header-nav">
                <span class="btn-icon-wrapper">
                    <i class="fa fa-ellipsis-v fa-w-6"></i>
                </span>
            </button>
        </span>
    </div>
    <div class="scrollbar-sidebar">
        <div class="app-sidebar__inner">
            <ul class="vertical-nav-menu">
                <li class="app-sidebar__heading">Menu</li>
                <li class="@yield('homes')">
                    <a href="#" class="@yield('homes')">
                        <i class="metismenu-icon pe-7s-rocket"></i>
                        Home
                        <i class="metismenu-state-icon pe-7s-angle-down caret-left"></i>
                    </a>
                    <ul >
                      <li><a href="{{url('/home')}}" class="@yield('homes')"><i class="metismenu-icon"></i> <span>Home</span></a></li>
                    </ul>
                    <ul>
                    </ul>
                </li>
                @if(Auth::user()->checkrelasi(Auth::id(),array('admin','karyawan')))
                  <li class="@yield('rules')@yield('users')">
                    <a href="#" class="@yield('rules')@yield('users')">
                      <i class="metismenu-icon pe-7s-folder"></i>
                      Master
                      <i class="metismenu-state-icon pe-7s-angle-down caret-left"></i>
                    </a>
                    <ul class="mm-collapse">
                      <!-- <li><a href="{{url('/tahun')}}" class="@yield('tahun')"><i class="metismenu-icon"></i> <span>Tahun</span></a></li> -->
                      <li><a href="{{url('/rules')}}" class="@yield('rules')"><i class="metismenu-icon"></i> <span>Rule</span></a></li>
                      <li><a href="{{url('/users')}}" class="@yield('users')"><i class="metismenu-icon"></i> <span>User</span></a></li>
                    </ul>
                  </li>
                @endif
            </ul>
        </div>
    </div>
</div>
