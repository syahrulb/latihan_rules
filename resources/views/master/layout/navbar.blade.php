<div class="app-header header-shadow bg-focus header-text-light">
  <div class="app-header__logo">
      <div class="logo-src"></div>
      <div class="header__pane ml-auto">
          <div>
              <button type="button" class="hamburger close-sidebar-btn hamburger--elastic" data-class="closed-sidebar">
                  <span class="hamburger-box">
                      <span class="hamburger-inner"></span>
                  </span>
              </button>
          </div>
      </div>
  </div>
  <div class="app-header__mobile-menu">
      <div>
          <button type="button" class="hamburger hamburger--elastic mobile-toggle-nav">
              <span class="hamburger-box">
                  <span class="hamburger-inner"></span>
              </span>
          </button>
      </div>
  </div>
  <div class="app-header__menu">
    <span>
      <button type="button" class="btn-icon btn-icon-only btn btn-primary btn-sm mobile-toggle-header-nav">
        <span class="btn-icon-wrapper">
            <i class="fa fa-ellipsis-v fa-w-6"></i>
        </span>
      </button>
    </span>
  </div>
  <div class="app-header__content">
      <div class="app-header-left">
      </div>
      <div class="app-header-right">
          <div class="header-btn-lg pr-0">
              <div class="widget-content p-0">
                  <div class="widget-content-wrapper">
                      <div class="widget-content-left">
                          <div class="btn-group">
                              <a href="#" data-toggle="dropdown" @click="tampilHeaderProfile()" aria-haspopup="true" aria-expanded="false" class="p-0 btn">
                                  <img width="42" class="rounded-circle" src="{{asset('assets/images/user.png')}}" alt="">
                                  <i class="fa fa-angle-down ml-2 opacity-8"></i>
                              </a>
                              <div id="dropdown-menu-profile" tabindex="-1" role="menu" aria-hidden="true" class="rm-pointers dropdown-menu-lg dropdown-menu dropdown-menu-right">
                                  <div class="dropdown-menu-header">
                                      <div class="dropdown-menu-header-inner bg-info">
                                          <div class="menu-header-image opacity-2" style="background-image: url({{asset('assets/images/dropdown-header/city3.jpg')}});"></div>
                                          <div class="menu-header-content text-left">
                                              <div class="widget-content p-0">
                                                  <div class="widget-content-wrapper">
                                                      <div class="widget-content-left mr-3">
                                                          <img width="42" class="rounded-circle"
                                                               src="{{asset('assets/images/user.png')}}"
                                                               alt="">
                                                      </div>
                                                      <div class="widget-content-left">
                                                          <div class="widget-heading">{{ Auth::user()->name }}
                                                          </div>
                                                          <div class="widget-subheading opacity-8">-
                                                          </div>
                                                      </div>
                                                  </div>
                                              </div>
                                          </div>
                                      </div>
                                  </div>
                                  <ul class="nav flex-column">
                                      <li class="nav-item-divider mb-0 nav-item"></li>
                                  </ul>
                                  <div class="grid-menu grid-menu-2col">
                                      <div class="no-gutters row">
                                          <div class="col-sm-6">
                                              <button class="btn-icon-vertical btn-transition btn-transition-alt pt-2 pb-2 btn btn-outline-warning">
                                                  <i class="pe-7s-chat icon-gradient bg-amy-crisp btn-icon-wrapper mb-2"></i>
                                                  Message Inbox
                                              </button>
                                          </div>
                                          <div class="col-sm-6">
                                              <button class="btn-icon-vertical btn-transition btn-transition-alt pt-2 pb-2 btn btn-outline-danger">
                                                  <i class="pe-7s-ticket icon-gradient bg-love-kiss btn-icon-wrapper mb-2"></i>
                                                  <b>Support Tickets</b>
                                              </button>
                                          </div>
                                      </div>
                                  </div>
                                  <ul class="nav flex-column">
                                      <li class="nav-item-divider nav-item">
                                      </li>
                                      <li class="nav-item-btn text-center nav-item">

                                        <a class="btn-wide btn btn-danger btn-sm" href="{{ route('logout') }}"
                                           onclick="event.preventDefault();
                                                         document.getElementById('logout-form').submit();">
                                            {{ __('keluar') }}
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            @csrf
                                        </form>
                                      </li>
                                  </ul>
                              </div>
                          </div>
                      </div>
                      <div class="widget-content-left  ml-3 header-user-info">
                          <div class="widget-heading">
                              {{Auth::user()->name}}
                          </div>
                          <div class="widget-subheading">
                              VP People Manager
                          </div>
                      </div>
                      <div class="widget-content-right header-user-info ml-3">
                      </div>
                  </div>
              </div>
          </div>

      </div>
  </div>
</div>
