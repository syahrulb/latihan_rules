<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Models\Rule;
class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','username'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

  	public function rules() {
  		return $this->belongsToMany('App\Models\Rule', 'rule_users', 'user_id', 'rule_id');
  	}
    public function checkrelasi($id,$arrayrule)
    {
      $iduser = $this->find($id)->id;
      foreach ($arrayrule as $key => $value) {
        $id = Rule::whereNama($value)->first()->id;
        $total= User::whereHas('rules',function ($query) use($id,$iduser) {
          $query->where('rule_id', $id)->where('user_id',$iduser);
        })->count();
        if ($total!=0) {
          return true;
          break;
        }
      }
      return false;
    }
}
