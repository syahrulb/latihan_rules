<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Auth;
use App\Models\Rule;
use Illuminate\Http\Request;

class RulesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;
        if (!empty($keyword)) {
            $rules = Rule::latest()->paginate($perPage);
        } else {
            $rules = Rule::latest()->paginate($perPage);
        }
        return view('rules.index', compact('rules'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('rules.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
  		$rules = [
  			'nama' => 'required|string|max:191',
        'status' => 'required',
  		];
      $nicename = array(
        'nama' => "Nama",
        'status' => "Status",
      );
  		$messages = [
  			'required' => ':attribute harus diisi',
  			'min' => ':attribute minimal  :min karakter.',
  			'max' => ':attribute maksimal  :max karakter.',
  			'numeric' => ':attribute harus berupa angka',
  			'unique' => ':attribute sudah ada sebelumnya.',
  			'required_if' => ':attribute harus diisi jika :other bernilai :value',
  		];
  		$validator = \Validator::make($request->all(), $rules, $messages);
  		if ($validator->fails()) {
  			return redirect()->back()->with('errors', $validator->errors())
  				->withInput();
  		}
      $requestData = $request->all();
      $total = Rule::where('nama',$requestData['nama'])->count();
      if ($total != 0) {
        return Redirect()->back()->with('peringatan','Nama Rule telah terdata pada sistem')->withInput();
      }
      $ip = $request->ip();
      $datas['ipcreatorsetting'] = $ip;
      $datas['ipupdatesetting'] = $ip;
      $datas['usercreatorsetting'] = Auth::user()->id;
      $datas['userupdatesetting'] = Auth::user()->id;
      Rule::create($requestData);
      return Redirect('rules')->with('berhasil','Data Rule telah tersimpan');
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $rule = Rule::findOrFail($id);
        return view('rules.show', compact('rule'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $rule = Rule::findOrFail($id);
        return view('rules.edit', compact('rule'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
      $rules = [
        'nama' => 'required',
        'status' => 'required',
      ];
      $nicename = array(
        'nama' => "Nama",
        'status' => "Status",
      );
      $messages = [
        'required' => ':attribute harus diisi',
        'min' => ':attribute minimal  :min karakter.',
        'max' => ':attribute maksimal  :max karakter.',
        'numeric' => ':attribute harus berupa angka',
        'unique' => ':attribute sudah ada sebelumnya.',
        'required_if' => ':attribute harus diisi jika :other bernilai :value',
      ];
      $validator = \Validator::make($request->all(), $rules, $messages, $nicename);
      if ($validator->fails()) {
        return redirect()->back()->with('errors', $validator->errors())
          ->withInput();
      }
      $requestData = $request->all();
      $model = Rule::findOrFail($id);
      $total = Rule::where('nama',$requestData['nama'])->where('id','!=',$model->id)->count();
      if ($total != 0) {
        return Redirect()->back()->with('peringatan','Nama Rule telah terdata pada sistem')->withInput();
      }
      $rule = Rule::findOrFail($id);
      $ip = $request->ip();
      $datas['ipupdatesetting'] = $ip;
      $datas['userupdatesetting'] = Auth::user()->id;
      $rule->update($requestData);
      return Redirect('rules')->with('berhasil','Data Rule telah diubah');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Rule::destroy($id);
        return Redirect('rules')->with('berhasil','Data Rule telah dihapus');
    }
}
